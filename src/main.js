import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Invitation from './Invitation.vue'

Vue.use(VueRouter)

const router = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '*',
      component: Invitation,
      props: (route) => ({
        to: route.query.to,
        stage: route.query.stage,
        bg: route.query.bg
      })
    }
  ]
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
